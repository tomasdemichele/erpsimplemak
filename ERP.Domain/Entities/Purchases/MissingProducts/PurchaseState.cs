﻿namespace ERP.Domain.Entities.Purchases.MissingProducts
{
    public class PurchaseState
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
