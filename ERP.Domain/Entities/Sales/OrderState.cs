﻿namespace ERP.Domain.Entities.Sales
{
    public class OrderState
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
